﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public static class CollectionUtils
{
    public static bool IsValidIndex<T>(this List<T> list, int index)
    {
        return index >= 0 && index < list.Count;
    }
}

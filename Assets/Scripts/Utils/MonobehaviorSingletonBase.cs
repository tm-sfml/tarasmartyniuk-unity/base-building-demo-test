﻿using UnityEngine;

public class MonobehaviorSingletonBase<T> : MonoBehaviour 
    where T : MonobehaviorSingletonBase<T>
{
    public static T Instance 
    { 
        get
        {
            if (m_instance == null)
            {
                m_instance = FindObjectOfType<T>();
            }
            return m_instance;
        }
    }

    static T m_instance;

    protected void Awake()
    {
        if (Instance != null && Instance != this as T)
        {
            Destroy(this);
        }
        else
        {
            m_instance = this as T;
        }
    }
}

﻿using UnityEngine;
using System.Collections.Generic;
using Unity.Collections;

public struct DebugOutputPrintCommand
{
    public string Message;
    public int Position; // denotes the persistent space on the screen for this message; offset from the top
    public bool IsPersistent;
    // position, size
}

public class OnScreenDebug : MonobehaviorSingletonBase<OnScreenDebug>
{
    List<DebugOutputPrintCommand> m_commands = new List<DebugOutputPrintCommand>();
    GUIStyle m_textAreaStyle;

    // outputs message in the top left corner at the specified position, 
    // if Persistent is true, message will be there until overwrited by this method or removed, else it stays only this frame
    // overrides the previous message for the Position
    public void Output(DebugOutputPrintCommand commandToPrint)
    {
        int index = m_commands.FindIndex(command => command.Position == commandToPrint.Position);
        if (index == -1)
        {
           m_commands.Add(commandToPrint);
        }
        else
        {
            m_commands[index] = commandToPrint;
        }
    }

    public void RemoveMessage(int position)
    {
        int index = m_commands.FindIndex(command => command.Position == position);
        m_commands.RemoveAt(index);
    }

    void Start()
    {
         m_textAreaStyle = new GUIStyle
         {
             fontSize = 10
         };
    }

    // TODO: might have problems with arbitrary update order?
    private void Update()
    {
        m_commands.RemoveAll(command => !command.IsPersistent);
    }

    void OnGUI()
    {
        foreach (var command in m_commands)
        {
            Vector2 size =  m_textAreaStyle.CalcSize(new GUIContent { text = command.Message });
            float verticalOffset = size.y * command.Position;
            GUI.Label(new Rect(0, verticalOffset, Screen.width - 10, 10), command.Message, m_textAreaStyle);
        }
    }
}

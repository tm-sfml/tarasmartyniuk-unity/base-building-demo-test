﻿using UnityEngine;

public class DebugOutputWriter : MonoBehaviour
{
    void Update()
    {
        var worldTiles = TileMapData.Instance.GetComponent<WorldTileMap>();
        var tile = worldTiles.GetTileUnderCursor();
        OnScreenDebug.Instance.Output(new DebugOutputPrintCommand { Message = $"Tile: {tile}", Position = 6, IsPersistent = true });
    }
}

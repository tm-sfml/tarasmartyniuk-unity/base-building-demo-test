﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopPageController : MonoBehaviour
{
    [SerializeField] BuildableItemSpawner BuildableSpawner;
    [SerializeField] GameObject Grid;
    [SerializeField] HudController HudController;

    void Start()
    {
        if (Grid == null)
        {
            Debug.LogError("Grid is null");
            return;
        }
        if (BuildableSpawner == null)
        {
            Debug.LogError("Spawner is null");
            return;
        }
        if (HudController == null)
        {
            Debug.LogError("HudController is null");
            return;
        }
        

        for (int i = 0; i < Grid.transform.childCount; i++)
        {
            Transform uiTile = Grid.transform.GetChild(i);

            if (!uiTile.TryGetComponent(out Button button))
            {
                Debug.LogError("no button on uitile");
                continue;
            }

            button.onClick.AddListener(() =>
            {
                var buildableTileComponent = button.GetComponent<BuildableItemUITile>();
                HudController.OnShopClosed();
                BuildableSpawner.StartPlacingBuildable(buildableTileComponent.BuildableItemId);
            });
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class HudController : MonoBehaviour
{
    [SerializeField] GameObject TileMapGrid;
    [SerializeField] GameObject Hud3D;
    [SerializeField] GameObject ShopPage;
    [SerializeField] TextMeshProUGUI ResourcesLabel;
    [SerializeField] CameraMovement CameraMovement;

    public void OnToggleGrid()
    {
        if (TileMapGrid == null)
        {
            Debug.LogError("TileMapGrid is null");
            return;
        }
        TileMapGrid.SetActive(!TileMapGrid.activeSelf);
    }

    public void OnShopButtonClicked() => ChangeIsShopPageDisplayed(true);

    public void OnShopClosed() => ChangeIsShopPageDisplayed(false);

    void ChangeIsShopPageDisplayed(bool isDisplayed)
    {
        if (Hud3D != null)
        {
            Hud3D.SetActive(!isDisplayed);
        }
        if (ShopPage != null)
        {
            ShopPage.SetActive(isDisplayed);
        }
        if (CameraMovement == null)
        {
            Debug.LogError("CameraMovement is null");
            return;
        }
        CameraMovement.enabled = !isDisplayed;
    }

    void Start()
    {
        if (Hud3D == null)
        {
            Debug.LogError("Hud3D is null");
            return;
        }
        if (ShopPage == null)
        {
            Debug.LogError("ShopPage is null");
            return;
        }
        Hud3D.SetActive(true);
        ShopPage.SetActive(false);
    }

    void Update()
    {
        if (ResourcesLabel == null)
        {
            Debug.LogError("ResourcesLabel is null");
            return;
        }

        var resources = PlayerResourcesStorage.Instance;
        ResourcesLabel.text = $"Wood: {resources.Wood}\nStone:{resources.Stone}";
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BorderForestGenerator : MonoBehaviour
{
    [SerializeField] GameObject TreePrefab;
    // in tiles, from inner tilemap border
    [SerializeField] int ForestStartDistance;
    [SerializeField] int ForestEndDistance;
    // % of tile radius
    [SerializeField] [Range(0, 1)] float MinRandomDisplacementPercent;
    [SerializeField] [Range(0, 1)] float MaxRandomDisplacementPercent;

    int ForestDepth => ForestEndDistance - ForestStartDistance;

    public void GenerateForest()
    {
        if (!TileMapData.Instance.TryGetComponent(out WorldTileMap worldTiles))
        {
            Debug.LogError("no WorldTileMap");
            return;
        }
        HierarchyUtils.DestroyChildren(gameObject, true);

        var tileMap = TileMapData.Instance;

        Vector2Int topRightInnerBorder = tileMap.Dimensions + Vector2Int.one * ForestStartDistance;
        Vector2Int topRightOuterBorder = topRightInnerBorder + Vector2Int.one * ForestEndDistance;
        Vector2Int botLeftInnerBorder = Vector2Int.zero - Vector2Int.one * ForestStartDistance;
        Vector2Int botLeftOuterBorder = botLeftInnerBorder - Vector2Int.one * ForestEndDistance;

        // spawns tree in a rectangular stripe
        void PlaceTreeStripe(Vector2Int botLeft, Vector2Int topRight)
        {
            for (int i = botLeft.x; i < topRight.x; i++)
            {
                for (int j = botLeft.y; j < topRight.y; j++)
                {
                    PlaceTree(new Vector2Int(i, j), worldTiles);
                }
            }
        }

        // right
        PlaceTreeStripe(new Vector2Int(topRightInnerBorder.x, botLeftOuterBorder.y), topRightOuterBorder);
        // left
        PlaceTreeStripe(botLeftOuterBorder, new Vector2Int(botLeftInnerBorder.x, topRightOuterBorder.y));
        // top
        PlaceTreeStripe(new Vector2Int(botLeftInnerBorder.x, topRightInnerBorder.y), topRightOuterBorder + Vector2Int.left * ForestDepth);
        // bot
        PlaceTreeStripe(botLeftOuterBorder + Vector2Int.right * ForestDepth, new Vector2Int(topRightInnerBorder.x, botLeftInnerBorder.y));
    }


    void OnDrawGizmos()
    {
        if (!TileMapData.Instance.TryGetComponent(out WorldTileMap worldTiles))
        {
            Debug.LogError("no WorldTileMap");
            return;
        }
        var tileMap = TileMapData.Instance;

        Vector2Int topRightInnerBorder = tileMap.Dimensions + Vector2Int.one * (ForestStartDistance - 1);
        Vector2Int topRightOuterBorder = topRightInnerBorder + Vector2Int.one * ForestEndDistance;
        Vector2Int botLeftInnerBorder = Vector2Int.zero - Vector2Int.one * ForestStartDistance;
        Vector2Int botLeftOuterBorder = botLeftInnerBorder - Vector2Int.one * ForestEndDistance;

        Gizmos.color = Color.green;
        void Draw(Vector2Int coords)
        {
            Gizmos.DrawLine(MathUtils.To3D(worldTiles.GetTileCenter(coords)),
                MathUtils.To3D(worldTiles.GetTileCenter(coords)) + Vector3.up * 2);
            //Debug.DrawLine(, Color.green, 20);
        }
        Draw(topRightInnerBorder);
        Draw(topRightOuterBorder);
        Draw(botLeftInnerBorder);
        Draw(botLeftOuterBorder);
    }

    void PlaceTree(Vector2Int coords, WorldTileMap worldTiles)
    {
        var randomAngle = UnityEngine.Random.Range(0, 360);
        Debug.Assert(MaxRandomDisplacementPercent > MinRandomDisplacementPercent);
        var percents = UnityEngine.Random.Range(MinRandomDisplacementPercent, MaxRandomDisplacementPercent);
        var displacement = MathUtils.Vector2FromAngle(randomAngle) * (worldTiles.TileSize / 2).magnitude * percents;

        var tree = Instantiate(TreePrefab, 
            MathUtils.To3D(worldTiles.GetTileCenter(coords)) + MathUtils.To3D(displacement),
            Quaternion.identity, transform);
    }
}

﻿using System;
using System.Collections.Generic;
using UnityEngine;

// MxN buildables are defined by their left-bottom most tile 
public class TileMapData : MonobehaviorSingletonBase<TileMapData>
{
    // refact: make a [SerializeField] + public {get; private set;} property
    [SerializeField] public Vector2Int Dimensions;

    // map {buildable ID(as int): data definition}
    [SerializeField] List<BuildableData> BuildableDataDefinitions;

    // 2D array; map {tile coordinate: data}
    // invalid if nothing placed
    // for MxN tiles, each tile has the same ID
    List<EBuildableItem> m_tileMap;

    // is the tile inside the square defined by dimensions?
    public bool IsInnerTile(Vector2Int coords)
    {
        return coords.x >= 0 && coords.x < Dimensions.x &&
            coords.y >= 0 && coords.y < Dimensions.y;
    }    

    public EBuildableItem GetBuildableAtCoordinate(Vector2Int coord)
    {
        int index = FlattenIndex(coord);
        return m_tileMap.IsValidIndex(index) ? m_tileMap[index] : EBuildableItem.Invalid;
    }

    // if buildable is MxN, fills all necessary tiles, coord is the bottom-left tile of such object
    public void AddBuildable(Vector2Int startCoords, EBuildableItem buildableId)
    {
        (BuildableData definition, bool success) = GetBuildableDataDefinition(buildableId);
        if (!success)
        {
            return;
        }

        for (int i = 0; i < definition.Dimensions.x; i++)
        {
            for (int j = 0; j < definition.Dimensions.y; j++)
            {
                Vector2Int coords = startCoords + new Vector2Int(i, j);
                Debug.Assert(GetBuildableAtCoordinate(coords) == EBuildableItem.Invalid);
                m_tileMap[FlattenIndex(coords)] = buildableId;
            }
        }
    }

    public (BuildableData, bool success) GetBuildableDataDefinition(EBuildableItem buildableId)
    {
        int index = (int)buildableId;
        if (!BuildableDataDefinitions.IsValidIndex(index))
        {
            Debug.LogError($"no def data for buildable id: {buildableId}");
            return default;
        }

        return (BuildableDataDefinitions[index], true);
    }

    void Start()
    {
        int flattenedSize = Dimensions.x * Dimensions.y;
        m_tileMap = new List<EBuildableItem>(flattenedSize);
        for (int i = 0; i < flattenedSize; i++)
        {
            m_tileMap.Add(EBuildableItem.Invalid);
        }
    }


    int FlattenIndex(Vector2Int coords)
    {
        return coords.x * Dimensions.x + coords.y;
    }
}

﻿using UnityEngine;
using UnityEngine.Tilemaps;

public class WorldTileMap : MonoBehaviour
{
    public Vector2 TileSize;

    public Vector2 GetTileCenter(Vector2Int coords)
    {
        return new Vector2(transform.position.x, transform.position.z) + 
            coords * TileSize;
    }

    public Vector2Int GetTileUnderCursor()
    {
        var tileMapPlane = new Plane(inNormal: Vector3.up, d: transform.position.y);
        Vector3 point = MathUtils.Project(Camera.main.ScreenPointToRay(Input.mousePosition), tileMapPlane);
        return GetClosestTile(new Vector2(point.x, point.z));
    }

    public Vector2Int GetClosestTile(Vector2 positionXZ)
    {
        int x = Mathf.RoundToInt(positionXZ.x / TileSize.x);
        int y = Mathf.RoundToInt(positionXZ.y / TileSize.y);
        return new Vector2Int(x, y);
    }
}
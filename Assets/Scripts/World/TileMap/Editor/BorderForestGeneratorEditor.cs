﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(BorderForestGenerator))]
public class BorderForestGeneratorEditor : Editor
{

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        var generator = (BorderForestGenerator)target;

        if (GUILayout.Button("Generate Forest"))
        {
            generator.GenerateForest();
        }
    }
}

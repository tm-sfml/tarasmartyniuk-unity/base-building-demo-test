﻿using System;
using UnityEngine;

public enum EBuildableItem
{
    Tree, Stone, Count, Invalid
}

public enum EBuildableResource
{
    Wood, CutStone, Count, Invalid
}

[Serializable]
public struct BuildableData
{
    public string Name;
    public Vector2Int Dimensions;
    public EBuildableResource ResourceType;
    public int ResourceYieldAmount;
}

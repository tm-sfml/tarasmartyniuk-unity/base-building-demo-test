﻿using System.Collections.Generic;
using UnityEngine;

struct BuildablePlacingData
{
    public GameObject Object;
    public EBuildableItem Id;
    public List<Color> UntintedColors;

    public void Reset()
    {
        Object = null;
        Id = EBuildableItem.Invalid;
        UntintedColors.Clear();
    }
}

public class BuildableItemSpawner : MonoBehaviour
{
    // map { buildableId(as int): prefab }
    [SerializeField] List<GameObject> Prefabs;

    public bool IsSpawnInProgress { get; private set; }

    BuildablePlacingData m_buildableToPlace;
    bool wasButtonDown;

    void Start()
    {
        m_buildableToPlace.UntintedColors = new List<Color>(5);    
    }

    public void StartPlacingBuildable(EBuildableItem buildableID)
    {
        if (m_buildableToPlace.Object != null)
        {
            Debug.LogError("already placing buildable");
            return;
        }

        int index = (int) buildableID;
        // exception could be used here
        if (!Prefabs.IsValidIndex(index))
        {
            Debug.LogError($"no prefab for buildable: {buildableID}");
            return;
        }

        var prefab = Prefabs[index];
        m_buildableToPlace.Object = Instantiate(prefab);
        m_buildableToPlace.Id = buildableID;
    }

    void Update()
    {
        if (m_buildableToPlace.Object == null)
        {
            return;
        }

        if (TileMapData.Instance == null)
        {
            Debug.LogError("TileMapData is null");
            return;
        }
        if (!TileMapData.Instance.TryGetComponent(out WorldTileMap worldTiles))
        {
            Debug.LogError("WorldTileMap is missing");
            return;
        }
        Vector2Int tileCoords = worldTiles.GetTileUnderCursor();

        if (!TileMapData.Instance.IsInnerTile(tileCoords))
        {
            return;
        }

        (BuildableData buildableData, bool success) = TileMapData.Instance.GetBuildableDataDefinition(m_buildableToPlace.Id);
        if (!success)
        {
            return;
        }

        bool currentTileValid = IsValidStartTile(tileCoords, buildableData.Dimensions);
        if (m_buildableToPlace.Object.TryGetComponent(out MeshRenderer renderer))
        {
            Material[] materials = renderer.materials;

            if (m_buildableToPlace.UntintedColors.Count == 0)
            {
                foreach (var material in materials)
                {
                    m_buildableToPlace.UntintedColors.Add(material.color);
                }
            }

            if (currentTileValid)
            {

                for (int i = 0; i < materials.Length; i++)
                {
                    materials[i].color = m_buildableToPlace.UntintedColors[i];
                }
            }
            else
            {
                foreach (var material in materials)
                {
                    material.color = Color.red;
                }
            }
        }
        
        Vector3 tileCenterPosition = MathUtils.To3D(worldTiles.GetTileCenter(tileCoords));
        tileCenterPosition += MathUtils.To3D(worldTiles.TileSize * (buildableData.Dimensions - new Vector2Int(1, 1))/ 2);
        m_buildableToPlace.Object.transform.position = tileCenterPosition;

        if (Input.GetMouseButtonDown(0))
        {
            wasButtonDown = true;
        }

        if (currentTileValid && wasButtonDown && Input.GetMouseButtonUp(0))
        {
            TileMapData.Instance.AddBuildable(tileCoords, m_buildableToPlace.Id);
            m_buildableToPlace.Reset();
            wasButtonDown = false;
        }
    }

    void LateUpdate()
    {
        // updating here to prevent unlocking dependent objects in the same frame
        IsSpawnInProgress = m_buildableToPlace.Object != null;
    }

    bool IsValidStartTile(Vector2Int startTileCoords, Vector2Int dimensions)
    {
        for (int i = 0; i < dimensions.x; i++)
        {
            for (int j = 0; j < dimensions.y; j++)
            {
                Vector2Int coords = startTileCoords + new Vector2Int(i, j);
                if(!TileMapData.Instance.IsInnerTile(coords) ||
                    TileMapData.Instance.GetBuildableAtCoordinate(coords) != EBuildableItem.Invalid)
                {
                    return false;
                }
            }
        }
        return true;
    }
}

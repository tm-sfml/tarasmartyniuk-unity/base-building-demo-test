﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(WorldBounds))]
public class WorldBoundsEditor : Editor
{

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        var bounds = (WorldBounds) target;

        if (GUILayout.Button("Setup"))
        {
            bounds.Setup();
        }
    }
}

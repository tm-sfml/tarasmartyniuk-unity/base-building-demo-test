﻿using Newtonsoft.Json;
using System.IO;
using UnityEngine;
using Newtonsoft.Json.Linq;

public class PlayerResourcesStorage : MonobehaviorSingletonBase<PlayerResourcesStorage>
{
    [SerializeField] string ResoursesFilename;

    public int Wood { get; set; }
    public int Stone { get; set; }

    void Start()
    {
        try
        {
            string filename = Path.Combine(Application.streamingAssetsPath, ResoursesFilename);
            string resourcesString = File.ReadAllText(filename);
            dynamic resourcesJson = JsonConvert.DeserializeObject(resourcesString);
            Wood = resourcesJson["wood"].ToObject<int>();
            Stone = resourcesJson["stone"].ToObject<int>();
        }
        catch (IOException e)
        {
            Debug.LogError(e);
            throw;
        }
    }

    void OnApplicationQuit()
    {
        using (var file = new StreamWriter(File.OpenWrite(Path.Combine(Application.streamingAssetsPath, ResoursesFilename))))
        {
            dynamic obj = new JObject();
            obj.wood = Wood;
            obj.stone = Stone;

            JsonSerializer serializer = new JsonSerializer();
            serializer.Serialize(file, obj);
        }
    }

}

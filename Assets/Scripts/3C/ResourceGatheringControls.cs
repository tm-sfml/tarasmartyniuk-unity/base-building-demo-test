﻿using UnityEngine;
using System.Collections;
using System;
using TMPro;
using UnityEditorInternal;

public class ResourceGatheringControls : MonoBehaviour
{
    [SerializeField] BuildableItemSpawner Spawner;
    LayerMask m_buildableLayerMask;

    void Start()
    {
        m_buildableLayerMask = LayerMask.GetMask("Buildable");
    }

    void Update()
    {
        if (Spawner == null)
        {
            Debug.LogError("Spawner is null");
        }
        if (Spawner.IsSpawnInProgress || !Input.GetMouseButtonUp(0))
        {
            return;
        }

        Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition),
            out RaycastHit hitResult, maxDistance: float.MaxValue, m_buildableLayerMask);
        if (hitResult.collider == null)
        {
            return;
        }

        if (!TileMapData.Instance.TryGetComponent(out WorldTileMap worldTiles))
        {
            return;
        }

        var buildable = hitResult.collider.gameObject;
        // get tile under
        Vector2Int tile = worldTiles.GetClosestTile(MathUtils.To2D(buildable.transform.position));
        Debug.Assert(TileMapData.Instance.IsInnerTile(tile));

        EBuildableItem buildableId = TileMapData.Instance.GetBuildableAtCoordinate(tile);
        (BuildableData buildableData, bool success) = TileMapData.Instance.GetBuildableDataDefinition(buildableId);
        if (!success)
        {
            return;
        }

        switch (buildableData.ResourceType)
        {
            case EBuildableResource.Wood:
                PlayerResourcesStorage.Instance.Wood += buildableData.ResourceYieldAmount;
                break;
            case EBuildableResource.CutStone:
                PlayerResourcesStorage.Instance.Stone += buildableData.ResourceYieldAmount;
                break;
            default:
                Debug.LogError("invalid resource type");
                break;
        }
    }
}

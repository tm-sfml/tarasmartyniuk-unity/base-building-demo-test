﻿using UnityEngine;

public class CameraMovement: MonoBehaviour
{
    [SerializeField] float Speed;
    [SerializeField] float ZoomSpeed;
    //[SerializeField] float MinFieldOfView;
    //[SerializeField] float MaxFieldOfView;

    [SerializeField] float MinZoomHeight = 1;
    [SerializeField] float MaxZoomHeight = 20;
    //float m_currentFieldOfView;

    void Start()
    {
        
    }

    void Update()
    {
        if (!TryGetComponent(out Rigidbody rigidbody))
        {
            return;
        }

        if (Input.GetMouseButton(0))
        {
            // move camera respecting its forward vector, but only on XZ plane
            Vector2 cameraMoveDelta = - new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
            Vector3 forwardXZ = Vector3.ProjectOnPlane(transform.forward, Vector3.up).normalized;
            Vector3 displacementDirection = forwardXZ * cameraMoveDelta.y + 
                transform.right * cameraMoveDelta.x;


            rigidbody.position += displacementDirection * Speed * transform.position.y * Time.deltaTime;
        }

        float scroll = Input.GetAxis("Mouse ScrollWheel");
        if (scroll != 0f)
        {
            Vector3 cameraPosition = transform.position;
            float newY = cameraPosition.y - scroll * ZoomSpeed * Time.deltaTime;
            cameraPosition.y = Mathf.Clamp(newY, MinZoomHeight, MaxZoomHeight);
            rigidbody.position = cameraPosition;
        }
    }
}

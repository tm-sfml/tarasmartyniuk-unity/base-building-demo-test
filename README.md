Intro
======
This is a test assignment for my Unity3D interview that I completed during 2020.08.01 - 2020.08.04.


What you can do:
=========
The project is a proto-demo of a base building game, similar to the numerous mobile "farms".
Camera movement is done by mouse: use mouse drag to move and scroll wheel to zoom.

Shop button shows the shop page, from where you can place unlimited objects on the grid, one at a time. You can then click on them to gather Wood and Stone; your total resources are displayed in the top left. They are automatically saved when exiting game/ending play in editor, and loaded on the start.

![Image](Screens/main_view.png)


How it is designed
=======

Generating Forest
-------
Done by BorderForestGenerator component. You define a square ring of tiles, and the tree is placed on each - with a random displacement. Works on editor time only, via the inspector "Generate Forest" button.

![Image](Screens/forest_gen.png)


Tile Map Data
------
Every object that can occupy a tile (Tree or Stone) is defined in TileMapData component:

![Image](Screens/tilemap.png)

Since our objects don't have any per-instance data, their properties are constant "definitions" - and are stored only once for each type. 
For each tile, only an ID is stored, using which I retrieve a corresponding definition from their array (Id Enum is used as an array index).
To find corresponding tile data, I use a flattened 2D array, where tile coordinates (Vector2Int) corresponds to an index of it's data in an array.

Saving/Loading
-----
Player resources data is stored in StreamingAssets/resources.json

Debug Overlay
-----
DebugOutputWriter class is used to print persistent debug messages currently showing the tile coordinates under cursor. You can disable it by disabling DebugOutputWriter game object in the editor.

Things to change:
-------
- Better images for UI :)
- more robust camera bounds system - checking world coordinates of screen corners? - instead of using the colliders


